import requests
import re
import os
import sys
import http.client
import sqlite3
import logging
import getopt


#Global variables :
from sys import platform
forceDownload = False
outputdir = './'

def chunks(l, n):
    '''
    Used for multi-threading
    '''
    for i in range(0, len(l), n):
        yield l[i:i + n]


def multi(dois, num_of_cores):
    '''
    Share the work with multi-threading.

    dois -- the list of dois to download
    num_of_cores -- the number of cores to use
    '''
    u = int(len(dois)/(num_of_cores*1.0))
    pool = mp.Pool(processes=num_of_cores)
    pool.map(func=work, iterable=chunks(dois, u))
    pool.close()
    pool.join()

def sql_update_fail(doi,c,conn):
    '''
    Update the db in case of failure.
    '''
    try:
        # downloaded column = 1, failed column = 0
        sanitized_doi = re.sub('/', '_', str(doi))
        c.execute("INSERT INTO journals (doi,downloaded,failed) VALUES (?,0,1)", (sanitized_doi,))
        conn.commit()
    except sqlite3.IntegrityError:
        logging.error('Error inserting failure into SQLite table')
        sys.exit()
    logging.info("Failure has been registered in the DB, future attempt for this DOI will be skipped")

def sql_update_success(doi,c,conn):
    '''
    Update the db in case of failure.
    '''
    try:
        # downloaded column = 1, failed column = 0
        sanitized_doi = re.sub('/', '_', str(doi))
        c.execute("UPDATE journals SET downloaded = 1 , failed = 0 WHERE doi = ?", (sanitized_doi,))
        conn.commit()
    except sqlite3.IntegrityError:
        logging.error('Error updating success into SQLite table')
        sys.exit()

def download_file_from_url(doi):
    '''
    Try to download doi's corresponding pdf.

    Return True if download has succeeded, False otherwise.
    '''
    doi = re.sub('[:]', '', str(doi))
    doi = re.sub(r'\s+', '+', str(doi))
    sanitized_doi = re.sub('/', '_', str(doi))
    # the link to download from (TODO : add other possibilities, and make it a parameter)
    my_link = 'http://booksdl.org/scimag/get.php?doi=' + doi
    logging.info("Downloading '{0}' from : '{1}'".format(str(doi), str(my_link)))
    try:
        r = requests.get(my_link,timeout=20)
        r.raise_for_status()
    except requests.exceptions.HTTPError as errh:
        logging.error(errh)
        logging.info("Failed to retrieve {0}, it is probably not present".format(doi))
        return False
    except requests.exceptions.ConnectionError as errc:
        logging.error(errc)
        return False
    except requests.exceptions.Timeout as errt:
         logging.error(errt)
         return False
    except requests.exceptions.RequestException as err:
         logging.error(err)
         return False
    with open(outputdir + sanitized_doi + '.pdf', 'wb') as fw:
        fw.write(r.content)
        logging.info(
            "Download completed. File stored here : '{0}'".format(outputdir))
        return True
    # If PDF saved, add it to database table.

def check_sql(sanitized_doi,c,conn):
    '''
    Check the sql database for the doi presence and state.

    Return
    - 1 if DOI is already in the table
    - 0 if it's nothing
    - -1 if it is and previous attempt at downloading failled
    '''
    try:
        # Retrieve row from DB for this DOI; parameter query to avoid SQL injection. Only one row, DOI is unique. No need to scan entire table.
        c.execute(
            "SELECT * FROM journals WHERE doi = ? LIMIT 1", (sanitized_doi,))
        # Extract the row returned.
        doi_row = c.fetchone()
        # if doi is in table
        if doi_row:
            # if DOI is downloaded
            if doi_row[2] == 1 and not forceDownload:
                logging.info(
                    "'{0}' - failed download last time per database record, skipping (use -F to force reattempt)".format(str(sanitized_doi)))
                return -1
            elif doi_row[1] == 1:
                # do nothing
                logging.info(
                    "'{0}' - already downloaded per database record".format(str(sanitized_doi)))
                # doi is in the table, download it
                return 1
        else:
            return 0
    # Handle SQl Exceptions
    except sqlite3.IntegrityError:
        logging.error("SQL Error")
        sys.exit()
        # File exists locally, skip.


def check_sql_consistency(sanitized_doi,c,conn):
    '''
    Check if sql db is consistent with the presence of the pdf in the directory.
    '''
    try:
        # Retrieve row from DB for this DOI; parameter query to avoid SQL injection. Only one row, DOI is unique. No need to scan entire table.
        c.execute(
            "SELECT * FROM journals WHERE doi = ? LIMIT 1", (sanitized_doi,))
        # Extract the row returned.
        doi_row = c.fetchone()
        #print(sanitized_doi)
        #print(doi_row)
        # if doi is in table
        if not doi_row:
            try:
                # downloaded column = 1, failed column = 0
                c.execute(
                    "INSERT INTO journals (doi,downloaded,failed) VALUES (?,1,0)", (sanitized_doi,))
                conn.commit()
            except sqlite3.IntegrityError:
                logging.error('Error inserting new row to SQLite table')
                sys.exit()
            logging.info("DOI added to database")
    except sqlite3.IntegrityError:
        logging.error("SQL Error")
        sys.exit()
            
def work(dois,c,conn):
    alreadyDL=0
    nsuccess=0
    nerror=0
    skipped=0
    for doi in dois:
        sanitized_doi = re.sub('/', '_', str(doi))
        if os.path.isfile(outputdir + sanitized_doi +'.pdf'):
            #file is already present in directory
            logging.info("There is already a pdf file in {0} for the doi : {1}".format(outputdir, sanitized_doi))
            logging.debug("Checking DB consistency")
            check_sql_consistency(sanitized_doi,c,conn)
            alreadyDL+=1
        else:
            dbState=check_sql(sanitized_doi,c,conn)
            if dbState==0:
                #doi is not in db
                if download_file_from_url(doi):
                    nsuccess+=1
                else:
                    nerror+=1
                    sql_update_fail(doi,c,conn)
            elif dbState==-1:
                #doi is in db and download failed last time
                if forceDownload:
                    if download_file_from_url(doi):
                        nsuccess+=1
                        sql_update_succes(doi,c,conn)
                    else:
                        nerror+=1
                else:
                    skipped+=1
            else:
                #doi is in db but is not in directory
                if download_file_from_url(doi):
                    nsuccess+=1
                else:
                    nerror+=1
    logging.info(" ================= Batch Finished ===================== ")
    print()
    logging.info("There was {0} skipped articles due to precedent attempt failure".format(skipped))
    logging.info("There was {0} already downloaded articles".format(alreadyDL))
    logging.info("{0} articles failed to download".format(nerror))
    logging.info("{0} new articles have been downloaded".format(nsuccess))

def usage():
    if platform == "linux" or platform == "linux2":
        start = "\033[1m"
        end = "\033[0;0m"
    else:
        start,end="",""
    # Windows...
    
    
    print(start+"Usage: "+end)
    print("    "+sys.argv[0]+" [-h]")
    print(
        "    "+sys.argv[0]+" [-i DOIsFile] [-o outputDir] [-l logLevel] [-L logOutput] [-c cores] [-F]")
    print(start+"Description :"+end)
    print("")
    print(start+"Options : "+end)
    print("    -h or --help 			: Display this message")
    print()
    print("    -i inputFile or --input=inputFile 	: Use 'inputFile' as a list of DOIs, input file should be a text file with a DOI on each line.")
    print("					  (DOIs.txt is used by default)")
    print()
    print("    -o outputDir or --output=outputDir 	: Use 'outputDir' to store the pdf, the sqlite database and the log (if no other file is specified for the log),")
    print("					  (current directory is used by default)")
    print()
    print("    -l logLevel or --log=logLevel 	: Change the level of logging used (possible values are DEBUG, INFO, WARNING, ERROR, CRITICAL. INFO is default value.")
    print("					  Lower/upper-case doesn't matter (debug and DEBUG both work)")
    print()
    print("    -L logFile or --logOutput=logFile 	: Specify where to store the logs (batch.log in output directory by default)")
    print()
    print("    -F or --force                       : Force new attempts for DOIs that failed in a preceding request")
    print()


def main(argv):
    if platform == "linux" or platform == "linux2":
        start = "\033[1m"
        end = "\033[0;0m"
    else:
        start,end="",""
    # Windows...
    print('                    ------------------------')
    print('                    - ' + start + 'DOI Batch Downloader' + end + ' -')
    print('                    ------------------------')
    print()
    print('Made by S. Juhel, modified from https://github.com/Nottt/Batch-DOI-Downloader')
    print('Feel free to use and modify.')
    print()

    # Get opts from command line :
    try:
        opts, args = getopt.getopt(argv, "hl:i:o:L:c:F", [
                                   "help", "log=", "input=", "output=", "logOutput=", "cores=", "force"])
        if not opts:
            print('[Warning] : No options supplied, using default values')
    except (getopt.GetoptError):
        # print(e)
        usage()
        sys.exit(2)

    # default values
    loglevel = 'INFO'
    inputfile = 'DOIs.txt'
    logOutput = './batch.log'
    maxcores = 1
    global forceDownload
    global outputdir

    for opt, arg in opts:
        if opt in ("-h", "--help"):
            usage()
            sys.exit()
        elif opt in ("-l", "--log"):
            loglevel = str(arg)
        elif opt in ("-i", "--input"):
            inputfile = str(arg)
        elif opt in ("-o", "--output"):
            outputdir = os.path.join(arg,'')
            logOutput = os.path.join(outputdir,"batch.log")
        elif opt in ("-L", "--logOutput"):
            logOutput = str(arg)
        elif opt in ("-c", "--cores"):
            maxcores = int(arg)
        elif opt in ("-F", "--force"):
            forceDownload=True
        source = "".join(args)

    # Logging config
    numeric_level = getattr(logging, loglevel.upper(), None)
    if not isinstance(numeric_level, int):
        raise ValueError('Invalid log level: %s' % loglevel)

    # Output directory must have trailing slash.
    outputdir=os.path.join(outputdir, '')
    # Check to see if output directory exists.
    if not os.path.isdir(outputdir):
        while True:
            createDir = input("Directory '{0}' doesn't exist, create it ? (y/n) : ".format(outputdir))
            if createDir not in ("y","n"):
                print("Sorry, I didn't understand that.")
                #better try again... Return to the start of the loop
                continue
            else:
                break
        if (createDir == "y") :
            try:
                os.makedirs(outputdir)
            except FileExistsError:
                # directory already exists
                pass
        else:
            sys.exit()

    logging.basicConfig(format='%(asctime)s %(levelname)s: %(message)s',
                        filename=logOutput, level=numeric_level)

    # define a Handler which writes INFO messages or higher to the sys.stderr
    console = logging.StreamHandler()
    console.setLevel(logging.INFO)
    # set a format which is simpler for console use
    formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
    # tell the handler to use this format
    console.setFormatter(formatter)
    # add the handler to the root logger
    logging.getLogger('').addHandler(console)

    logging.info(" =============== New batch download =================== ")
    #os.system('cat cdnt.txt | sort | uniq > cdnt.txt')

    file_list_of_ids = inputfile

    if_mp = (maxcores > 1)
    num_of_cores = maxcores
    if if_mp:
        import multiprocessing as mp

    # Open SQLite database and create table if it does not already exist. Failed column is for future use, if there is a need to skip DOIs that always fail.
    logging.info("Looking for sql base or creating it")
    conn = sqlite3.connect(outputdir+'doi_log.sqlite')
    c = conn.cursor()
    try:
        c.execute(
            '''CREATE TABLE IF NOT EXISTS journals (doi TEXT PRIMARY KEY, downloaded INTEGER, failed INTEGER);''')
    except sqlite3.IntegrityError:
        logging.error('Error creating SQLite table')
        sys.exit()

    conn.commit()

    with open(file_list_of_ids, 'r') as f:
        dois = [line.rstrip() for line in f]
    #with open('cdnt.txt', 'r') as f:
    #    not_dois = [line.rstrip() for line in f]
    #    dois = [x for x in dois if x not in not_dois]

        if if_mp == 'True':
            multi(dois, num_of_cores)
        else:
            work(dois, c, conn)


if __name__ == "__main__":
    main(sys.argv[1:])
