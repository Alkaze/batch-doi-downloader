# Description
This repository hosts a simple, short script that allows batch .PDF downloading from a list of DOIs and/or titles. PDF files are retrieved/downloaded from libgen.

An sql database is created and store the information of already downloaded pdfs. 

# See also

This version is a modified version of https://github.com/Nottt/Batch-DOI-Downloader 
# Requirements

Recommended sqlite3 version : 2.6.0

Python version used : 3.7

Python packages used :
* `urllib`
* `requests`
* `sqlite3`
* `logging`

# Usage and options

## Basic use

The downloader.py script can be used from command line, as such:

`python downloader.py [-i DOIsFile] [-o outputDir] [-l logLevel] [-L logOutput] [-c cores] [-F]` 

The script does the following :

* It checks if `outputDir` exists (`./` by default) and ask to create it if it doesn't.
* It looks for `doi_log.sqlite` inside `outputDir` and try to create it if it doesn't exist, otherwise it connect to the database.
* It reads `DOIsFile` as an UTF8 text file containing one DOI per line.
* For each DOI, it checks `outputDir` to see if the file already exists (and update the database if necessary), then check the database to see if there has been failed previous attempts (and if so, skip the DOI if `-F` option is not specified).
* When those tests have been made, it attempt to download the pdf from libgen into `outputDir` and update the database accordingly.

Log from the script are appended to `batch.log` inside `outputDir`.

The script will detect if the 'multiprocessing' module is installed, and will ask you for the number of cores you may want to put into use (just to speed up the http request / .PDF downloading process).

## Options

Here are different options you may use :
* `-h` or `--help` : display this help.
* `-i inputFile` or `--input=inputFile` : Use 'inputFile' as a list of DOIs, input file should be a text file with a DOI on each line. (DOIs.txt is used by default).
* `-o outputDir` or `--output=outputDir`  : Use 'outputDir' to store the pdf, the sqlite database and the log (if no other file is specified for the log), (current directory is used by default).
* `-l logLevel` or `--log=logLevel`       : Change the level of logging used (possible values are `DEBUG`, `INFO`, `WARNING`, `ERROR`, `CRITICAL`. `INFO` is default value. Lower/upper-case doesn't matter (debug and DEBUG both work).
* `-L logFile` or `--logOutput=logFile`   : Specify where to store the logs (batch.log in output directory by default).
* `-F` or `-force` : force new attempts for DOIs that failed during a preceding request.
